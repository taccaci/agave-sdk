# Agave Client SDK
*******

This is the parent project for the [Agave Clinet SDK](http://agaveapi.co/client-sdk/). The individual projects are linked as submoldules of this project. To check out all the SDK, run the following:

	$ git submodule init
	$ git submodule update
	
For information on the individual SDK, please see the `README.md` files in each of the project folders. 

If you would like to contribute to the development of any of the SDK, please fork the project and issue a pull reqeust. If you would like to contribute another language or publicize your own Agave client library, please send an email to us on the [iPlant Agave Developers Mailing List](mailto:iplant-api-dev@iplantcollaborative.org)